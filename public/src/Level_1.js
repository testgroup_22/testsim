$(function () {
	
	// Level 1 Implementation - 
	
	var validateJson = {};
	var i = 0;
	var count = $('#wrapper').find('select').length;
	var selectCount = 0;
	console.log(count);
	
	// logic to hide option after selection
	
	$("select.positionTypes").change(function () {
		
	    $("select.positionTypes option[value='" + $(this).data('index') + "']").show();
	    $(this).data('index', this.value);
	    $("select.positionTypes option[value='" + this.value + "']:not([value=''])").hide();
	    
	    });
	
	
	//  on change store the answer in json 
	
	$(".positionTypes").change(function() {

		var selected = $("option:selected", $(this)).val();
        
		var thisID = $(this).attr("id");
        
        $(".positionTypes").each(function() {
            if ($(this).attr("id") == thisID) {
            	selectCount++;
            }
        });
        
        
    	if(selected == thisID){
    		validateJson[i++] = true +'_'+thisID;
    	}else{
    		validateJson[i++] = false +'_'+thisID;
    	}
        	

    });
	
	
	// function to check whether student identify components correct or not.
	
	$("#identify").click(function(){
		
		if(selectCount != count){
        	alert("Please select the remaining elements");
				        } else {

					for (key in validateJson) {
						var imgId = validateJson[key].split("_");
						console.log(imgId);
						if (imgId[0] == "true") {
							$("div#" + imgId[1]).addClass(
									"success glyphicon glyphicon-ok");
							// alert("all objects valid");
						} else {
							flag = false;
							$("#" + imgId[1]).addClass(
									"danger glyphicon glyphicon-remove");
						}
					}
				};
			
		
		
	});
	
// to reset the level 1
	$("#reset").click(function(){
		window.location.reload(true);
	});
	
	
		

});   //END LINE