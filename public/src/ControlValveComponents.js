
var ValveBody;

var DigramWidth = $("#diagram").width();


//  list of components of control valve
level1 = function() {
	
	
	var optionsHTM = '<option value="0">select option</option>'
					+'<option value="PositionerStem">Positioner Stem</option>'
					+'<option value="ValveBody">Valve Body</option>'
					+'<option value="Bonnet">Bonnet</option>'
					+'<option value="Spring">Spring</option>'
					+'<option value="plug">Plug</option>'
					+'<option value="seatedRing">Seated Ring</option>'
					+'<option value="Indicatorplate">Indicator plate</option>'
					+'<option value="gradelPlate">Gland Packing</option>'
					+'<option value="Actuator">Actuator</option>'
					+'<option value="ActuatorStem">Actuator Stem</option>'
	$(".positionTypes").append(optionsHTM);
	
}

// method to draw all components

identifyValve = function() {	
	
	
 var ValveBodyPaper = new Raphael(document.getElementById('ValveBody'), 350 ,220 );

 var BonnetPaper = new Raphael(document.getElementById('Bonnet'), 350 ,220 );
 
 var PlugPaper = new Raphael(document.getElementById('plug'), 350 ,220 );
 
 var SeatedRingPaper = new Raphael(document.getElementById('seatedRing'), 350 ,220 );
 
 var PositionerStemPaper = new Raphael(document.getElementById('PositionerStem'), 350 ,220 );
 
 var GlandPackingPaper = new Raphael(document.getElementById('gradelPlate'), 350 ,220 );
 
 var IndicatorplatePaper = new Raphael(document.getElementById('Indicatorplate'), 350 ,220 );
 
 var DiaphragmActuatorPaper = new Raphael(document.getElementById('Spring'), 350,220 );

 var Actuatorpaper = new Raphael(document.getElementById('Actuator'), 350,220 );
 
 var ActuatorStemPaper = new Raphael(document.getElementById('ActuatorStem'), 350,220 );
 
	x = 10 ;
	y = 0;

    var ValveBody = ValveBodyPaper.path('M' + (x+40) + ' ' + (y+210) + 'l 0 -70 28 0 42 40 85 0 0 -44 8 0 0 -20 15 4 0 15 68 0 0 75 -28 0 0 -45 -20 0 -25 45 -105 0 -40 -42 0 42 z'
			+ 'M' + (x+ 40) + ' ' + (y+10) + 'l 0 70 72 0 0 50 18 0 0 -5 -5 0 0 -78 -7 0 0 -15  -5 0  0 15 -4 0 0 -15 -7 0 0 38 -30 0 0 -60 z'	
			+'M' + (x+ 285) + ' ' + (y+10) + 'l 0 85 -25 0 -58 -10 0 -38 8 0 0 -15 4 0 0 15 5 0 0 -15 7 0 0 35 30 0 0 -55 z'
			).attr        ({
				"stroke-width" : 2,
						fill : "#958a6e"
				});	

    var bonnetUL= BonnetPaper.path('M' + (x+120) + ' '  +(y+60) + 'l 5 0 0 9 5 0 0 -9 5 0 0 7 5 0 0 110 -20 0 0 -15 -8 0 0 -15 -18 0 0 -20 5 0 0 8  0 12 5 0 0 -20 16 0 z ' 
			 + 'M' + (x+190) + ' '  +(y+60) + 'l -5 0  0 9 -5 0 0 -9 -5 0 0 7 -5 0 0 110 20 0 0 -15 8 0 0 -15 18 0 0 -20 -5 0 0 20 -5 0 0 -20 -16 0 Z' 
			 +  'M' + (x+147) + ' '  +(y+63) + 'l -9 0 0 -7 -18 0 0 -12 5 0 0 12 5 0 0 -12 17 0 z' 
			 + 'M' + (x+163) + ' '  +(y+63) + 'l 9 0 0 -7 18 0 0 -12 -5 0 0 12 -5 0 0 -12 -17 0 z').attr({ 
			"stroke-width" : 2,
				fill : "#958a6e"
			});


	
	var Plug = PlugPaper.path('M100,50 C100,130 200,130 200,50 L 100 50')
	.attr({"stroke-width": 2, stroke: "#000" ,fill : "#436161"});
	
		var seatRing = SeatedRingPaper.path( /*seatRing bottom start*/'M' + (x+130) + ' '  +(y+94) + 'l 0 15 18 0 -5 -15 z ' + 'M' + (x+180) + ' '  +(y+94) + 'l 0 15 -17 0 5 -15 z '/*seatRing bottom end*/ /*seatRing top start*/ + 'M' + (x+130) + ' '  +(y+50) + 'l 0 12 10 0 -3 -12 z ' + 'M' + (x+180) + ' '  +(y+50) + 'l 0 12 -10 0 3 -12 z '/*seatRing top end*/).attr({ 
		"stroke-width" : 2,
			fill : "#999",
			
		});
		

	var PositionerStem = PositionerStemPaper.path('M' + (x+150) + ' '  +(y+220) + 'l 8 0 0 -150 15 0 0 -15 10 0 -15 0 10 -8 0 -20 0 5 -5 -5 -5 0 -5 5 0 5 5 5 5 0 5 -5 0 -20 -10 -8 -30 0 -10 8 0 15 5 -5 5 0 5 5 0 5 -5 5 -5 0 -5 -5 0 -7 0 17 10 8 -15 0 10 0 0 15 8 0 0 -15 25 0 0 15 -8 0 0 -15 -8 0 0 15 -8 0 0 -8 -8 0 40 0 -24 0 0 8 -5 0 5 0 z' ).attr({ 
	"stroke-width" : 2,
		fill : "#436161"
	});

//	var PositionerStem = PositionerStemPaper.path('M ' + (x+150) + ' '  +(y+20) + ' l 45 0 l 10 12 l 0 35 l -12 10 l -43 0 l -12 -10 l 0 -35 z').attr({ 
//			"stroke-width" : 2,
//				fill : "#e2e2e2"
//			});
//	
//	var stemLine =  PositionerStemPaper.path('M ' + (x + 124) + ' '  +(y+77) + ' l 95 0 l 0 1.5 l -95 0 z').attr({ 
//		"stroke-width" : 2,
//		fill : "#e2e2e2"
//	});
//	
//	
//	var actualStem =  PositionerStemPaper.path('M ' + (x + 168) + ' '  +(y+79) + ' l 0 150 l 10 0 l 0 -150 ').attr({ 
//		"stroke-width" : 2,
//		fill : "#e2e2e2"
//	});
//	
//	var c1 = PositionerStemPaper.circle((x+ 146 ) ,(y+50) , 7).attr({ 
//		"stroke-width" : 2,
//		fill : "#fff"
//	});
//	
//	var c2 = PositionerStemPaper.circle((x + 198), (y + 50), 7).attr({ 
//	"stroke-width" : 2,
//	fill : "#fff"
//	});

	var grandpackSet = GlandPackingPaper.set();
	
	var GlandPackingUL = GlandPackingPaper.path( 'M' + (x+170) + ' '  +(y+80) + 'l -13 0 0 -30 13 0 0 30'  ).attr({ 
		"stroke-width" : 2,
		fill : "#a52a2a"	
		});
	
	var GlandPackingUR = GlandPackingPaper.path( 'M' + (x+144) + ' '  +(y+80) + 'l -13 0 0 -30 13 0 0 30'  ).attr({ 
		"stroke-width" : 2,
		fill : "#a52a2a"	
		});
	
	 var GlandPackingflowR  = GlandPackingPaper.path( 'M' + (x+170) + ' '  +(y+130) + 'l -13 0 0 -50 13 0 0 50'  ).attr({ 
		"stroke-width" : 2,
		fill : "#666"	
		});

	 var GlandPackingflowL  = GlandPackingPaper.path( 'M' + (x+144) + ' '  +(y+130) + 'l -13 0 0 -50 13 0 0 50'  ).attr({ 
			"stroke-width" : 2,
			fill : "#666"	
			});
	 
	 var GlandPackingBL = GlandPackingPaper.path( 'M' + (x+144) + ' '  +(y+150) + 'l -13 0 0 -30 13 0 0 30'  ).attr({ 
			"stroke-width" : 2,
			fill : "#a52a2a"	
			});
		
	 var GlandPackingBR = GlandPackingPaper.path( 'M' + (x+170) + ' '  +(y+150) + 'l -13 0 0 -30 13 0 0 30'  ).attr({ 
			"stroke-width" : 2,
			fill : "#a52a2a"	
			});
		
	 grandpackSet.push(GlandPackingUL,GlandPackingUR,GlandPackingflowL,GlandPackingflowL,GlandPackingBL,GlandPackingBR);
	
	
	 var Indicatorplate = IndicatorplatePaper.path('M' + (x+140) + ' '  +(y+50) + 'l 15 0 0 50 -15 0 0 -7 8 0 -8 0 0 -7 5 0 -5 0 0 -7 8 0 -8 0 0 -7 5 0 -5 0 0 -7 8 0 -8 0 0 -7 5 0 -5 0 0 -7' ).attr({ 
		"stroke-width" : 2,
		fill : "#d2b48c"
		});	

	
// Diaphragm Actuator start 

	
	var DiaphragmActuator = DiaphragmActuatorPaper.set();
		

		 	   
		var springs = DiaphragmActuatorPaper.set();
		 
		   
		var  sspring1 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+50) + 'l 70 -10 2 10 -70 10 z' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
		   
			});	 
		
		
		var  sspring2 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+77) + 'l 70 -10 2 10 -70 10 z ' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
			});	 
		  
		var  sspring3 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+107) + 'l 70 -10 2 10 -70 10 z ' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
			});	 
		var  sspring4 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+132) + 'l 70 -5 2 10 -70 5 z' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
			});	 
		
		var  sspring5 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+50) + 'l 70 16 0 10 -70 -16 z' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
			});	
		sspring5.toBack()
		var  sspring6 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+73) + 'l 70 20 0 10 -70 -20 z ' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
			});	 
		sspring6.toBack()
		var  sspring7 = DiaphragmActuatorPaper.path('M' + (x+120) + ' '  +(y+108) + 'l 70 20 0 10 -70 -20 z ' ).attr({ 
			"stroke-width" : 2,
		    "stroke" : "#000",
		    fill : "#958a6e"
			});	
		 
		sspring7.toBack()
			   springs.push(sspring1,sspring2,sspring3,sspring4,sspring5,sspring6,sspring7);
		   
		DiaphragmActuator.push(springs)
		
	  
// Diaphragm Actuator end		
		
		var  Actuator =	Actuatorpaper.path(
		        'M40,90 C40,60 300,60 300,90 z'
		        ).attr({ 
				"stroke-width" : 2,
				fill : "#436161"
				});	 		

		
		var ActuatorStemSet = ActuatorStemPaper.set();
		var  ActuatorStem = ActuatorStemPaper.path('M' + (x+20) + ' '  +(y+20) + 'Q 225, 20 l 129 0 0 155 10 0 0 -155 129 0 z ' ).attr({ 
			"stroke-width" : 2,
			fill : "#436161"
			});	 

		var  SpringBase= ActuatorStemPaper.path('M' + (x+149) + ' '  +(y+201) + 'l 10 0 0 -26 0 10 10 0 0 -8 15 0 0 -5 -60 0 0 5 15 0 0 8 10 0 0 -13  z' ).attr({ 
	 		"stroke-width" : 2,
	 			fill : "#436161"
	 		});
		ActuatorStemSet.push(ActuatorStem,SpringBase);


};


var screwS = function(x,y)
{
	paper.path('M' + (x+16) + ' '  +(y+305) + 'l 2 0 0 40 -2 0  0 -40 -5 0 0 5 12 0 0 -5 -5 0 2 0 0 -3 -5 0 0 3 3 0 z' ).attr({ 
	"stroke-width" : 2,
		fill : "#9a9a9a"
	});
	
}

var screwL = function(x,y)
{
	paper.path('M' + (x+9) + ' '  +(y+355) + 'l 2 0 0 55 -2 0  0 -55 -5 0 0 5 12 0 0 -5 -5 0 2 0 0 -3 -5 0 0 3 3 0 z' ).attr({ 
	"stroke-width" : 2,
		fill : "#9a9a9a"
	});
}


var GlandPacking = function(x,y)
{
	paper.path( 'M' + (x+499) + ' '  +(y+364) + 'l -13 0 0 -30 13 0 0 30'  ).attr({ 
	"stroke-width" : 2,
	fill : "#a52a2a"	
	});
}

var GlandPackingflow = function(x,y)
{
	paper.path( 'M' + (x+499) + ' '  +(y+364) + 'l -13 0 0 -50 13 0 0 50'  ).attr({ 
	"stroke-width" : 2,
	fill : "#666"	
	});
}
