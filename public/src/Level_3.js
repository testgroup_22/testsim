$(function () {
// Level 3 Implementation
	
	Level_3_ValveSizing = function() {
		
		$("header").html("<img src='images/COEPlogo.png'/> <h1>COLLEGE OF ENGINEERING, PUNE</h1><h2><i>Control Valve Calculations</i></h2>");
		$("#main-div").html("");
		$("#main-div").css("padding", "10px 10%");
		
		$("#main-div").append("<div id = 'flowMediumDiv'></div><div id = 'flowMediumWaterDiv'></div></div>");
		
		$("#flowMediumWaterDiv").hide();
		var flowMediumhtm = '<br>'
			+'<div class="title_par" >Calculate the parameters required for sizing of a pneumatically operated control valve</div>';
		
		flowMediumhtm += '<div><label>Process fluid</label>'
							+ ' <select class="form-control" id="flowMedium" name="flowMedium" title="Choose Flow medium">'
							+ '<option value="-1">Choose Value</option>'
							for ( var key in processFluidJson) {
							
								flowMediumhtm  += "<option value= " + key + ">" + processFluidJson[key]
										+ "</option>";
							}
		
		flowMediumhtm  +=  '</select>'
			+'<legend id = "expTitle" class="exptitle"></legend>'
			+'</div>';
		
		$("#flowMediumDiv").html(flowMediumhtm);
		$("#expTitle").hide();
	
		// select the process fluids type
		$('select#flowMedium').on('change', function() {
			
			var flowMediumValue = $('#flowMedium option:selected').val();
			
			if(flowMediumValue == "-1"){
				
				$("#expTitle").html("");
				
				$("#flowMediumWaterDiv").html('');;
			}else if(flowMediumValue == "0"){
				
				$("#flowMediumWaterDiv").show();
				$("#expTitle").show();
				
				$("#expTitle").html("Determine the appropriate valve size for valve with linear cage where line size is 3 inch.");
				flowMediumWater();
			}
		});
	
}


	
});